FROM archlinux

ENV UID=1000
ENV GID=$UID
ENV USERN=leo
ENV UHOME=/home/$USERN
ENV APPHOME=$UHOME/.local/opt
ENV PATH=$UHOME/.local/bin:$PATH
ENV LUA_PATH=$UHOME/.config/nvim/?.lua

RUN pacman --noconfirm -Sy --needed archlinux-keyring && \
    pacman --noconfirm -Su && \
    pacman --noconfirm -S which sudo openssh tmux git less unzip ripgrep fd \
    fish fisher neovim starship ttf-font-nerd chezmoi \
    python go clojure nodejs npm && \
    groupadd -g $GID leo && \
    useradd -u $UID -g $GID -m -s /bin/fish $USERN

USER $USERN
WORKDIR $UHOME
RUN chezmoi init https://bitbucket.org/leechau/mydotfiles.git && \
    mkdir -p $APPHOME $UHOME/docs $UHOME/.config/fish && \
    chezmoi apply $UHOME/.config/fish && \
    git clone --filter=blob:none --branch=stable https://github.com/folke/lazy.nvim.git $UHOME/.local/share/nvim/lazy/lazy.nvim
RUN git clone https://bitbucket.org/leechau/lazy-configs.git $UHOME/.config/nvim && \
    ls -la $UHOME/.config/nvim && \
    nvim --headless '+Lazy! sync' +qa && \
    nvim --headless '+MasonInstall cljfmt debugpy gopls lua-language-server stylua pyright' +qa
ENTRYPOINT ["/usr/bin/fish"]