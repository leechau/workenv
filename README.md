# Working Environment in Container

## Changelog

* 0.5.3: Dockerfile-python is the working version based on Alpine.
  However, due to the performance issue of Python on Alpine, from 0.6
  the Python toolchain move back to Ubuntu.

